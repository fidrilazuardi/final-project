@extends('layout.master')

@section('judul')
Halaman Edit Varian {{$varian->nama}}
@endsection

@section('content')

<form action="/varianf/index/{{$varian->id}}" method="post">
	@csrf
	@method('put')
  <div class="form-group">
    <label >Varian : </label>
    <input type="text" name="nama" value="{{$varian->nama}}" class="form-control">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Gambar :</label>
    <input type="text" value="{{$varian->img}}" class="form-control" name="umur">
  </div>
  @error('img')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Update</button>
</form>

@endsection