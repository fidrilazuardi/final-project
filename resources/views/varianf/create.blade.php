@extends('layout.master')

@section('judul')
Sign Up Form
@endsection

@section('content')

<form action="/varianf/index" method="post">
	@csrf
  <div class="form-group">
    <label >Nama Varian : </label>
    <input type="text" name="nama" class="form-control">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Gambar :</label>
    <input type="text" class="form-control" name="img">
  </div>
  @error('img')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection