<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class VarianController extends Controller
{
    public function create(){
        return view('varianf.create');
    }

    public function store(Request $request){
        // dd($request->all());
        $request->validate([
        'nama' => 'required',
        'img' => 'required',
    ]);

    DB::table('varian')->insert([
    'nama' => $request['nama'],
    'img' => $request['img']
    ]);

    return redirect('/varianf/index');
    }

    public function index(){
        $varian = DB::table('varian')->get();
        return view('varianf.index', compact('varian'));
    }

    public function show($id){
        $varian = DB::table('varian')->where('id', $id)->first();
        return view('varianf.show', compact('varian'));
    }

    public function edit($id){
        $varian = DB::table('varian')->where('id', $id)->first();
        return view('varianf.edit', compact('varian'));
    }

    public function update($id, Request $request){
            $request->validate([
            'nama' => 'required',
            'img' => 'required',
        ]);

        $query = DB::table('varian')
              ->where('id', $id)
              ->update([
                    'nama' => $request['nama'],
                    'img' => $request['img']

              ]);
        return redirect('/varianf/index');
    }

    public function destroy($id){
        DB::table('varian')->where('id', $id)->delete();

        return redirect('/varianf/index');
    }
}
