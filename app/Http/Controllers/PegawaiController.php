<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\User;
use Hash;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = 1;
        $pegawai = Profile::all();
        $pegawai = Profile::whereHas('User', function ($q) use ($userId) {
        $q->where('is_admin', 1);
})->get();

        return view('pegawai.index', compact('pegawai'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pegawai.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'name' => 'required',
    		'email' => 'required',
            'password' => 'required',
    		'umur' => 'required',
            'alamat' => 'required',
    		'bio' => 'required',
    	]);
 
        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->password),
            'is_admin' => 1
        ]);


        $profile = Profile::create([
            'umur' => $request->umur,
            'alamat' => $request->alamat,
            'bio' => $request->bio,
            'user_id' => $user->id,
        ]);
 
    	 return redirect('/pegawai');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pegawai = Profile::find($id);
        $pegawai = Profile::where('user_id',$id)->first();
        return view('pegawai.show', compact('pegawai'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pegawai = Profile::find($id);
        $pegawai = Profile::where('user_id',$id)->first();
        return view('pegawai.edit', compact('pegawai'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
    		'email' => 'required',
    		'umur' => 'required',
            'alamat' => 'required',
    		'bio' => 'required',
        ]);

        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->update();

        $pegawai = Profile::find($id);
        $pegawai = Profile::where('user_id',$id)->first();
        $pegawai->umur = $request->umur;
        $pegawai->alamat = $request->alamat;
        $pegawai->bio = $request->bio;
        $pegawai->update();
        return redirect('/pegawai');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pegawai = Profile::find($id);
        $pegawai = Profile::where('user_id',$id)->first();
        $pegawai->delete();

        $user  = User::find($id);
        $user->delete();
 
        return redirect('/pegawai');
    }
}