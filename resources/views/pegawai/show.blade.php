@extends('layout.master')

@section('judul')

Detail Pegawi {{ $pegawai->name }}
@endsection

@section('content')
      <div class="text-center">
        <img class="profile-user-img img-fluid img-circle"
        src="{{ asset('img/'.$pegawai->img) }}"
             alt="User profile picture">
      </div>

      <h3 class="profile-username text-center">{{$pegawai->user->name }}</h3>

      <p class="text-muted text-center">{{$pegawai->user->email }}</p>

      <div class="card-body">
        <strong><i class="fas fa-calendar mr-1"></i> Umur</strong>

        <p class="text-muted">
            {{$pegawai->umur }} Tahun
        </p>

        <hr>

        <strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat</strong>

        <p class="text-muted">{{$pegawai->alamat }}</p>

        <hr>

        <strong><i class="far fa-file-alt mr-1"></i> Bio</strong>

        <p class="text-muted">{{$pegawai->bio }}</p>
      </div>
      <a href="/pegawai" class="btn btn-danger btn-block"><b>Back</b></a>
@endsection