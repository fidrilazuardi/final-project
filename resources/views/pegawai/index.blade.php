@extends('layout.master')

@section('judul')
List Pegawai
@endsection

@section('content')

<a href="/pegawai/create" class="btn btn-primary mb-3">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">email</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">alamat</th>
                <th scope="col">aksi</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($pegawai as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->user->name}}</td>
                        <td>{{$value->user->email}}</td>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->bio}}</td>
                        <td>{{$value->alamat}}</td>
                        <td>
                            <a href="/pegawai/{{$value->user->id}}" class="btn btn-info">Detail</a>
                            <a href="/pegawai/{{$value->user->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/pegawai/{{$value->user->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection