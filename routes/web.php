<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

// CRUD Produk
Route::get('/produk/create', 'ProdukController@create');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/addToCart/{produk_id}/{profile_id}', 'CartController@addTocart');
Route::get('/myCart/{user_id}', 'CartController@myCart');
Route::delete('/myCart/{cart_id}', 'CartController@destroy');
Route::get('adm/home', 'HomeController@admHome')->name('adm.home')->middleware('is_admin');

Route::group(['middleware' => ['is_admin']], function () {
    //CRUD Admin
    Route::get('/pegawai/create', 'PegawaiController@create');
    Route::post('/pegawai', 'PegawaiController@store')->name('pegawai.index');
    Route::get('/pegawai', 'PegawaiController@index');
    Route::get('/pegawai/{pegawai_id}', 'PegawaiController@show');
    Route::get('/pegawai/{pegawai_id}/edit', 'PegawaiController@edit');
    Route::put('/pegawai/{pegawai_id}', 'PegawaiController@update');
    Route::delete('/pegawai/{pegawai_id}', 'PegawaiController@destroy');

        // CRUD Varian
    Route::get('/varianf/create', 'VarianController@create');
    Route::post('/varianf/index', 'VarianController@store');
    Route::get('/varianf/index', 'VarianController@index');
    Route::get('/varianf/index/{varian_id}', 'VarianController@show');
    Route::get('/varianf/index/{varian_id}/edit', 'VarianController@edit');
    Route::put('/varianf/index{varian_id}', 'VarianController@update');
    Route::delete('/varianf/index/{varian_id}', 'VarianController@destroy');

        Route::post('/produk', 'ProdukController@store');
    Route::get('/produk', 'ProdukController@index');
    Route::get('/produk/{produk_id}', 'ProdukController@show');
    Route::get('/produk/{produk_id}/edit', 'ProdukController@edit');
    Route::put('/produk/{produk_id}', 'ProdukController@update');
    Route::delete('/produk/{produk_id}', 'ProdukController@destroy');
});



