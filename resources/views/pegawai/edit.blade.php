@extends('layout.master')

@section('judul')
Edit pegawai {{ $pegawai->name }}
@endsection
@push('script')
<script src="https://cdn.tiny.cloud/1/gzsgfpdf6kz5n5u0gnej43zjbza6pr1d0y7hdddsjfwboc27/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush
@section('content')

<div>
        <form action="/pegawai/{{ $pegawai->user->id }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label>name</label>
                <input type="text" class="form-control" value="{{ $pegawai->user->name }}" name="name" placeholder="Masukkan name">
                @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>email</label>
                <input type="text" class="form-control" value="{{ $pegawai->user->email }}" name="email" placeholder="Masukkan Email">
                @error('email')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>umur</label>
                <input type="number" class="form-control" value="{{ $pegawai->umur }}" name="umur" placeholder="Masukkan umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Alamat</label>
                <textarea name="alamat" class="form-control" cols="30" rows="10">{{ $pegawai->alamat }}</textarea>
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Bio</label>
                <textarea name="bio" class="form-control" cols="30" rows="10">{{ $pegawai->bio }}</textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
</div>

@endsection