<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProdukController extends Controller
{

    public function create(){
        return view('produk.create');
    }

    public function store(Request $request){
        $request->validate([
            'title' => 'required',
            'produk' => 'required',
        ]);

        DB::table('produk')->insert([
            'email' => $request['email'],
            'produk' => $request['produk']
        ]);

        return redirect('/produk/create');
    }

    public function index(){
        $produk = DB::table('produk')->get();
        return view('produk.index', compact('produk'));
    }

    public function show(){
        $produk =
    }
}
