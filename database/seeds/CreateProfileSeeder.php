<?php

use Illuminate\Database\Seeder;
use App\Profile;

class CreateProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $profile = [
            [
                'umur'=>'0',
                'bio'=>'Hello World',
                'alamat'=>'Bali',
                'user_id'=> '1',
            ],
            [
                'umur'=>'0',
                'bio'=>'World Hello',
                'alamat'=>'Papua',
                'user_id'=> '2',
            ],
        ];
    }
}
