<?php

namespace App\Http\Controllers;
use App\Produk;
use App\Cart;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $produk = Produk::all();
        $user = Auth::user();
        $cartCek = Cart::all();
        $cartCek = Cart::where('profile_id',$user->id)->first();
        if ( $cartCek === NULL) {
            $totalcart = 0;
        }else{
            $totalcart = $cartCek->sum('jumlah');
        }
        
    //     $totalcart = 0;
    //     foreach ($cartCek as $key) {
    //         $totalcart = $cartCek->jumlah;
    //     }

        return view('home', compact('produk','totalcart'));
    }

    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function admHome()
    {
        return view('admHome');
    }
    
}
