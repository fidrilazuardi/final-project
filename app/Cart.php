<?php

namespace App;
use App\Produk;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = "tmpkeranjang";
    protected $fillable = [ 'profile_id', 'produk_id' ,'jumlah',];

    public function produk()
    {
        return $this->belongsTo(Produk::class);
    }
}
