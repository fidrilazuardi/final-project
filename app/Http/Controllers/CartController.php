<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\Produk;
use Auth;

class CartController extends Controller
{
    public function addToCart($produk_id, $profile_id)
    {
        $cartCek = Cart::find($produk_id);
        $cartCek = Cart::where('profile_id',$profile_id)->first();
        if ($cartCek === null) {
            $cart = Cart::create([
                
                'profile_id' => $profile_id,
                'produk_id' => $produk_id,
                'jumlah' => 1,
            ]);
        } else {
            $jumlah = $cartCek->jumlah + 1;
            
            $cartCek->jumlah = $jumlah;
            $cartCek->update();
            
        }

        
 
    	 return redirect('home');
    }

    public function myCart($id)
    {
        $cart= Cart::all();
       
        $totalcart = 4;

        return view('cart.show', compact('cart','totalcart',));
    }

    public function destroy($id)
    {
        $cartdelete  = Cart::find($id);
        $cartdelete->delete();
        $user = Auth::user();
        
        return redirect('/myCart/{{ $user->id }}');
    }
}
