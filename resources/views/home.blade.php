@extends('layout.usermaster')
   
@section('content')
<!-- Start Varian  -->
<div class="categories-shop">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="shop-cat-box">
                    <img class="img-fluid" src="{{ asset('toko/images/categories_img_01.jpg') }}" alt="" />
                    <a class="btn hvr-hover" href="#">Lorem ipsum dolor</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="shop-cat-box">
                    <img class="img-fluid" src="{{ asset('toko/images/categories_img_02.jpg') }}" alt="" />
                    <a class="btn hvr-hover" href="#">Lorem ipsum dolor</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="shop-cat-box">
                    <img class="img-fluid" src="{{ asset('toko/images/categories_img_03.jpg') }}" alt="" />
                    <a class="btn hvr-hover" href="#">Lorem ipsum dolor</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Categories -->

<div class="box-add-products">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="offer-box-products">
                    <img class="img-fluid" src="{{ asset('toko/images/add-img-01.jpg') }}" alt="" />
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="offer-box-products">
                    <img class="img-fluid" src="{{ asset('toko/images/add-img-02.jpg') }}" alt="" />
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Start Products  -->

<div class="products-box">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="title-all text-center">
                    <h1>Produk Kue</h1>
                </div>
            </div>
        </div>
        <div class="row special-list">
            

@foreach ($produk as $key)
            @if ($loop->first)
                    <div></div>
            @endif

<div class="col-lg-3 col-md-6 special-grid best-seller">
    <div class="products-single fix">
        <div class="box-img-hover">
            <div class="type-lb">
                <p class="sale">Sale</p>
            </div>
            <img src="{{ asset('img/produk/'.$key->img) }}" class="img-fluid" alt="Image">
            <div class="mask-icon">
                
                <ul>
                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                </ul>
                <a class="cart" href="/addToCart/{{ $key->id }}/{{ Auth::user()->id }}">Add to Cart</a>
            </div>
        </div>
        <div class="why-text">
            <h4>{{$key->nama}}</h4>
            <h3>
                {{ $key->deskripsi }}
            </h3>
            <h5>Rp. {{$key->harga}}</h5>
        </div>
    </div>
</div>
@endforeach 
{{-- @forelse ($produk as $key=>$value)

                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->harga}}</td>
                        <td>{{$value->deskripsi}}</td>
                        <td>{{$value->img}}</td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse --}}

            

            
        </div>
    </div>
</div>
<!-- End Products  -->
@endsection